from django.urls import path
from . import views

app_name = 'stori5'

urlpatterns = [
    path('daftar_matkul/', views.matkul, name='matkul'),
    path('detail_matkul/<int:index>/', views.detail_matkul, name='detail_matkul'),
    path('tambah/', views.tambah_matkul, name='tambah_matkul'),
]

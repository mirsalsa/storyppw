from django.db import models

# Create your models here.

class Matkul(models.Model):
    subject = models.CharField(max_length=50)
    dosen = models.CharField(max_length=120)
    sks = models.IntegerField()
    deskripsi = models.TextField(blank=True)
    semester = models.CharField(max_length=120, help_text="2020/2021 ganjil")
    kelas = models.CharField(max_length=120, help_text="2204")

    def __str__(self):
        return self.subject
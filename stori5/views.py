from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .form import MatkulForm
from .models import Matkul
# Create your views here.
def matkul(request):
    if request.method == "POST":
        Matkul.objects.get(id=request.POST['id']).delete()
        return redirect('/daftar_matkul/')
    subjects = Matkul.objects.all()
    context = {'daftar_matkul':subjects}
    return render(request, 'daftar_matkul.html', context)
    

def tambah_matkul(request):
    submit = False
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/tambah/?submit=True')
    else:
        form = MatkulForm
        if 'submit' in request.GET:
            submit = True
    context = {'form': form, 'submit': submit}
    return render(request, 'tambah_matkul.html', context)

def detail_matkul(request, index):
    subject = Matkul.objects.get(pk=index)
    context = {'subject':subject}
    return render(request, 'detail_matkul.html', context)


    
    

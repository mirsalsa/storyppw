from django.forms import ModelForm
from .models import Matkul

class MatkulForm(ModelForm):
	required_css_class = 'required'
	class Meta:
		model = Matkul
		fields = '__all__'

from django.urls import path
from . import views

app_name = 'stori1'

urlpatterns = [
    path('stori1', views.home, name='home'),
]

from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'stori3.html')

def extra(request):
    return render(request, 'extra.html')

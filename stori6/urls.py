from django.urls import path
from . import views

app_name = 'stori6'

urlpatterns = [
    path('daftar_event/', views.event, name='event'),
    path('registrasi/<int:index>/', views.registrasi, name='registrasi'),
    path('tambah_event/', views.tambah_event, name='tambah_event'),
]
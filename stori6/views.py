from django.shortcuts import render, redirect
from .forms import EventForm, ParticipantForm
from .models import Event, Participant

# Create your views here.
def event(request):
    kegiatan = Event.objects.all()
    partisipan = Participant.objects.all()

    if request.method == "POST":
        Participant.objects.get(id=request.POST['id']).delete()
        return redirect('/daftar_event/')

    return render(request, 'daftar_event.html', {'daftar_event':kegiatan, 'partisipan':partisipan})

def tambah_event(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/daftar_event/')
    else:
        form = EventForm
    context = {'form': form}
    return render(request, 'tambah_event.html', context)

def registrasi(request, index):
    if request.method == 'POST':
        form = ParticipantForm(request.POST)
        if form.is_valid():
            partisipan = Participant(name=form.data['name'], event=Event.objects.get(id=index))
            partisipan.save()
            return redirect('/daftar_event/')
    else:
        form = ParticipantForm()
    context = {'form': form}
    return render(request, 'registrasi.html', context)


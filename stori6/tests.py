from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import event, tambah_event, registrasi
from .models import Event, Participant
from .apps import Stori6Config

# Create your tests here.
class TestingAplikasi(TestCase):
    def test_apakah_aplikasi_ada(self):
        self.assertEquals(Stori6Config.name, 'stori6')
        self.assertEquals(apps.get_app_config('stori6').name, 'stori6')

    def test_stori6_url_ada(self):
        response = Client().get('/daftar_event/')
        self.assertEquals(response.status_code, 200)

    def test_stori6_pake_template(self):
        response = Client().get('/daftar_event/')
        self.assertTemplateUsed(response, 'daftar_event.html')
    
    def test_stori6_modelEvent_ada(self):
        event = Event(name="coba")
        event.save()
        count = Event.objects.all().count()
        self.assertEquals(count, 1)

    def test_stori6_modelParticipant_ada(self):
        event = Event.objects.create(name="coba")
        partisipan = Participant.objects.create(name='kamu', event=event)
        
        count = Participant.objects.all().count()
        self.assertEquals(count, 1)

    def test_stori6_nama_model_ada(self):
        event = Event(name="coba")
        self.assertEquals(str(event), "coba")

    def test_stori6_nama_model_ada2(self):
        event = Event.objects.create(name="coba")
        partisipan = Participant.objects.create(name='kamu', event=event)
        self.assertEquals(str(partisipan), "coba - kamu")


    def test_stori6_model_bisa_jalan(self):
        event = Event.objects.create(name="coba")
        partisipan = Participant.objects.create(name='kamu', event=event)
        response = Client().post('/daftar_event/', data={'id':1})
        self.assertEquals(response.status_code, 302)

class TestingEvent(TestCase):
    def test_stori6_urlE_ada(self):
        response = Client().get('/tambah_event/')
        self.assertEquals(response.status_code, 200)

    def test_tambah_event_index_fungsi(self):
        found = resolve('/tambah_event/')
        self.assertEqual(found.func, tambah_event)

    def test_stori6E_pake_template(self):
        response = Client().get('/tambah_event/')
        self.assertTemplateUsed(response, 'tambah_event.html')

    def test_stori6_event_bisa_jalan(self):
        response = Client().post('/tambah_event/', data={'event':'coba'})
        self.assertEquals(response.status_code, 200)

class TestingRegistrasi(TestCase):
    def setUp(self):
        event = Event(name="coba")
        event.save()

    def test_stori6_regis_bisa_jalan(self):
        response = Client().post('/registrasi/1/', data={'name':'aku'})
        self.assertEquals(response.status_code, 302)

    def test_stori6_regis_pake_template(self):
        response = Client().get('/registrasi/1/')
        self.assertTemplateUsed(response, 'registrasi.html')



    



    
from django.db import models

# Create your models here.
class Event(models.Model) :
    name = models.CharField(max_length=128)
    def __str__(self) :
        return self.name

class Participant(models.Model) :
    name = models.CharField(max_length=128)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    def __str__(self) :
        return str(self.event) + " - " + self.name

